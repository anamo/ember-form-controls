/*! @anamo/ember-form-controls v1.0.0 | © 2006-present, Anamo Inc. MIT License | bitbucket.org/anamo/ember-form-controls */

import Component from '@ember/component'
import ModelFormMixin from '../mixins/model-form-mixin'
import layout from '../templates/components/form-feedback'

export default Component.extend(ModelFormMixin, {
	layout,
	tagName: 'div',
	classNames: 'form-feedback'.w(),
})
