/*! @anamo/ember-form-controls v1.0.0 | © 2006-present, Anamo Inc. MIT License | bitbucket.org/anamo/ember-form-controls */

import Helper from '@ember/component/helper'
import { bind, next } from '@ember/runloop'

/**
 * handleInput(event) {
 * if(event.target.value !== this.get('model.pagesPath')) {
 * next(this, function() {
 * event.target.value = this.get('model.pagesPath')
 * })
 * }
 * },
 */
export default Helper.extend({
	compute([...rest], hash) {
		return function (event) {
			event.preventDefault()
			next(this, bind(this, () => {
				this.get('model').notifyPropertyChange(this.get('prop'))
			}))
		}
	}
})
