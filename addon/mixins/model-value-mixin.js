/*! @anamo/ember-form-controls v1.0.0 | © 2006-present, Anamo Inc. MIT License | bitbucket.org/anamo/ember-form-controls */

import { on } from '@ember/object/evented'
import Mixin from '@ember/object/mixin'

export default Mixin.create({
	prop: undefined,

	bindModelValueChanged: on('didInsertElement', function () {
		this.addObserver('value', this, this.modelValueHasChanged)
	}),

	unbindModelValueChanged: on('willDestroyElement', function () {
		this.removeObserver('value', this, this.modelValueHasChanged)
	}),

	modelValueHasChanged(sender, key, value, rev) {
		if (value !== rev) this.trigger('modelValueChanged')
	},
})
