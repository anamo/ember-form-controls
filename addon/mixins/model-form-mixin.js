/*! @anamo/ember-form-controls v1.0.0 | © 2006-present, Anamo Inc. MIT License | bitbucket.org/anamo/ember-form-controls */

import { reads } from '@ember/object/computed'
import Mixin from '@ember/object/mixin'

export default Mixin.create({
	for: undefined,
	name: undefined,

	model: reads('for.model'),
	errors: reads('for.errors'),
})
