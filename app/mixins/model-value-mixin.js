/*! @anamo/ember-form-controls v1.0.0 | © 2006-present, Anamo Inc. MIT License | bitbucket.org/anamo/ember-form-controls */

export { default } from '@anamo/ember-form-controls/mixins/model-value-mixin';

