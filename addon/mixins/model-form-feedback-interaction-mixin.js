/*! @anamo/ember-form-controls v1.0.0 | © 2006-present, Anamo Inc. MIT License | bitbucket.org/anamo/ember-form-controls */

import { on } from '@ember/object/evented'
import Mixin from '@ember/object/mixin'
import { isEmpty } from '@ember/utils'

export default Mixin.create({
	resetServerInvalidityOnUserInteraction: on('input', 'change', 'paste', function () {
		if (isEmpty(this.get('for.serverValidities'))) return
		this.get('for.serverValidities').removeObjects(this.get('for.serverValidities').filterBy('source.pointer', `/data/${this.get('name')}`))
	})
})
