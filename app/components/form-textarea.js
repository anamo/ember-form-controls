/*! @anamo/ember-form-controls v1.0.0 | © 2006-present, Anamo Inc. MIT License | bitbucket.org/anamo/ember-form-controls */

export { default } from '@anamo/ember-form-controls/components/form-textarea';

