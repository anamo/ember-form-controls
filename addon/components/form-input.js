/*! @anamo/ember-form-controls v1.0.0 | © 2006-present, Anamo Inc. MIT License | bitbucket.org/anamo/ember-form-controls */

import TextField from '@ember/component/text-field'
import FormControlMixin from '../mixins/form-control-mixin'
import ModelValueMixin from '../mixins/model-value-mixin'

export default TextField.extend(FormControlMixin, ModelValueMixin, {})
