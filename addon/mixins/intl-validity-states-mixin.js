/*! @anamo/ember-form-controls v1.0.0 | © 2006-present, Anamo Inc. MIT License | bitbucket.org/anamo/ember-form-controls */

import { computed } from '@ember/object'
import Mixin from '@ember/object/mixin'

export default Mixin.create({
	overrideValidationMessages: false,

	'data-validity-msg-bad-input': computed('intl.locale', {
		get() {
			return this.intl.t('components.form-control.html-invalid-message.bad-input')
		}
	}),
	'data-validity-msg-custom-error': computed('intl.locale', {
		get() {
			return this.intl.t('components.form-control.html-invalid-message.custom-error')
		}
	}),
	'data-validity-msg-pattern-mismatch': computed('intl.locale', {
		get() {
			return this.intl.t('components.form-control.html-invalid-message.pattern-mismatch')
		}
	}),
	'data-validity-msg-range-overflow': computed('intl.locale', {
		get() {
			return this.intl.t('components.form-control.html-invalid-message.range-overflow')
		}
	}),
	'data-validity-msg-range-underflow': computed('intl.locale', {
		get() {
			return this.intl.t('components.form-control.html-invalid-message.range-underflow')
		}
	}),
	'data-validity-msg-step-mismatch': computed('intl.locale', {
		get() {
			return this.intl.t('components.form-control.html-invalid-message.step-mismatch')
		}
	}),
	'data-validity-msg-too-long': computed('intl.locale', {
		get() {
			return this.intl.t('components.form-control.html-invalid-message.too-long')
		}
	}),
	'data-validity-msg-too-short': computed('intl.locale', {
		get() {
			return this.intl.t('components.form-control.html-invalid-message.too-short')
		}
	}),
	'data-validity-msg-type-mismatch': computed('intl.locale', {
		get() {
			return this.intl.t('components.form-control.html-invalid-message.type-mismatch')
		}
	}),
	'data-validity-msg-value-missing': computed('intl.locale', {
		get() {
			return this.intl.t('components.form-control.html-invalid-message.value-missing')
		}
	})
})
