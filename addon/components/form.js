/*! @anamo/ember-form-controls v1.0.0 | © 2006-present, Anamo Inc. MIT License | bitbucket.org/anamo/ember-form-controls */

import Component from '@ember/component'
import FormMixin from '../mixins/form-mixin'
import layout from '../templates/components/form'

export default Component.extend(FormMixin, {
	layout,
	tagName: 'form',
})
