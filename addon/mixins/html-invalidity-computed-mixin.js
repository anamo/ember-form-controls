/*! @anamo/ember-form-controls v1.0.0 | © 2006-present, Anamo Inc. MIT License | bitbucket.org/anamo/ember-form-controls */

import { on } from '@ember/object/evented'
import Mixin from '@ember/object/mixin'

export default Mixin.create({
	resetHtmlInvalidityOnRemovalFromDOM: on('willDestroyElement', function () {
		this.trigger('becameValidNow') // NOTE: Every user interaction triggers this. User is FREE to edit it without error notifications.
	}),

	resetHtmlInvalidityOnUserInteraction: on('input', 'change', 'paste', 'modelValueChanged', function () {
		this.trigger('becameValid') // NOTE: Every user interaction triggers this. User is FREE to edit it without error notifications.
	}),

	bindHtmlInvalidityOnInvalidation: on('invalid', function () {
		let object = Object.assign({}, this.element.validity)
		this.trigger('becameInvalid', Object.keys(object).find(key => true === object[key]))
	}),
})
