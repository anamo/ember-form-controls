/*! @anamo/ember-form-controls v1.0.0 | © 2006-present, Anamo Inc. MIT License | bitbucket.org/anamo/ember-form-controls */

import Component from '@ember/component'
import FormControlMixin from '../mixins/form-control-mixin'

export default Component.extend(FormControlMixin, {
	tagName: 'select',
	attributeBindings: 'name required disabled tabindex autofocus'.w(),
})
