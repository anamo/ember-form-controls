/*! @anamo/ember-form-controls v1.0.0 | © 2006-present, Anamo Inc. MIT License | bitbucket.org/anamo/ember-form-controls */

import { A } from '@ember/array'
import EmberObject, { computed } from '@ember/object'
import { on } from '@ember/object/evented'
import Mixin from '@ember/object/mixin'
import { next } from '@ember/runloop'
import { dasherize } from '@ember/string'
import { isEmpty, isNone } from '@ember/utils'
import HtmlInvalidityComputedMixin from './html-invalidity-computed-mixin'
import IntlValidityStatesMixin from './intl-validity-states-mixin'
import ModelFormFeedbackInteractionMixin from './model-form-feedback-interaction-mixin'
import ModelFormMixin from './model-form-mixin'
import NativeInvalidMixin from './native-invalid-mixin'

export default Mixin.create(HtmlInvalidityComputedMixin, ModelFormMixin, ModelFormFeedbackInteractionMixin, IntlValidityStatesMixin, NativeInvalidMixin, {
	classNameBindings: 'isInvalid:is-invalid'.w(),

	isInvalid: computed('for', 'name', 'errors.[]', {
		get() {
			return !isNone((this.get('errors') || A()).findBy('group', this.get('name')))
		}
	}).readOnly(),

	passNativeValidityToFormInstantly: on('becameValidNow', function () {
		if (isEmpty(this.get('for')) || isEmpty(this.get('name')) || isEmpty(this.get('for.htmlValidities'))) return
		let existing = this.get('for.htmlValidities').findBy('attribute', this.get('name'))
		if (!isEmpty(existing)) this.get('for.htmlValidities').removeObject(existing)
	}),

	passNativeValidityToForm: on('becameValid', function () {
		next(this, () => {
			if (isEmpty(this.get('for')) || isEmpty(this.get('name')) || isEmpty(this.get('for.htmlValidities'))) return
			let existing = this.get('for.htmlValidities').findBy('attribute', this.get('name'))
			if (!isEmpty(existing)) this.get('for.htmlValidities').removeObject(existing)
		})
	}),

	passNativeInvalidityToForm: on('becameInvalid', function (htmlInvalidity) {
		if (isEmpty(this.get('for')) || isEmpty(this.get('name'))) return
		(this.get('for.htmlValidities').findBy('attribute', this.get('name')) || this.get('for.htmlValidities').pushObject(EmberObject.create({
			attribute: this.get('name'),
			message: undefined
		}))).
			set('message', this.get('overrideValidationMessages') ? this.get(`data-validity-msg-${dasherize(htmlInvalidity)}`) : this.element.validationMessage)
	}),

	passFocusToForm: on('focusIn', function () {
		if (isEmpty(this.get('for')) || isEmpty(this.get('name'))) return true
		this.set('for.currentFocus', this.get('name'))
	}),

	resetFocusFromForm: on('focusOut', function () {
		if (isEmpty(this.get('for'))) return true
		this.set('for.currentFocus', undefined)
	}),
})
