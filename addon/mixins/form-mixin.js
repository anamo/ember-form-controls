/*! @anamo/ember-form-controls v1.0.0 | © 2006-present, Anamo Inc. MIT License | bitbucket.org/anamo/ember-form-controls */

import { A } from '@ember/array'
import EmberObject, { computed } from '@ember/object'
import Mixin from '@ember/object/mixin'

export default Mixin.create({
	attributeBindings: 'novalidate',
	novalidate: 'novalidate',

	model: undefined,
	additionalModel1: undefined,
	additionalModel2: undefined,
	additionalModel3: undefined,

	currentFocus: undefined,

	htmlValidities: A(),

	serverValidities: A(),

	errors: computed('model.errors.[]', 'additionalModel1.errors.[]', 'additionalModel2.errors.[]', 'additionalModel3.errors.[]', 'htmlValidities.@each.{attribute,message}', 'serverValidities.[]', {
		get() {
			let groupedContent = A()

			this.get('htmlValidities').
				groupEach('attribute', groupedContent, item => item, this)

				;
			(this.get('model.errors') || A()).
				map(item => EmberObject.create(item), this).
				groupEach('attribute', groupedContent, item => item, this)

				;
			(this.get('additionalModel1.errors') || A()).
				map(item => EmberObject.create(item), this).
				groupEach('attribute', groupedContent, item => item, this)

				;
			(this.get('additionalModel2.errors') || A()).
				map(item => EmberObject.create(item), this).
				groupEach('attribute', groupedContent, item => item, this)

				;
			(this.get('additionalModel3.errors') || A()).
				map(item => EmberObject.create(item), this).
				groupEach('attribute', groupedContent, item => item, this)


			this.get('serverValidities').
				map(item => EmberObject.create({
					attribute: item.source.pointer.substring(6),
					message: item.detail
				}), this).
				groupEach('attribute', groupedContent, item => item, this)

			return groupedContent
		}
	}).readOnly(),

	submit(event) {
		event.preventDefault()
		event.stopPropagation()
		if ('novalidate' === this.get('novalidate')) {
			if (!this.element.checkValidity()) return
		}
		this.onValidSubmit(event)
	}
})
