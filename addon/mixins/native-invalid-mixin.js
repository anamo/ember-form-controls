/*! @anamo/ember-form-controls v1.0.0 | © 2006-present, Anamo Inc. MIT License | bitbucket.org/anamo/ember-form-controls */

import { on } from '@ember/object/evented'
import Mixin from '@ember/object/mixin'
import { bind, scheduleOnce } from '@ember/runloop'
import $ from 'jquery'

export default Mixin.create({
	bindInvalid: on('didInsertElement', function () {
		scheduleOnce('afterRender', this, function () {
			$(this.element).on('invalid', bind(this, this.invalidTrigger))
		})
	}),

	discardInvalid: on('willDestroyElement', function () {
		$(this.element).off('invalid')
	}),

	invalidTrigger() {
		this.trigger('invalid')
	},
})
